const livre1 = {
    titre : "Nos cabanes",
    auteur: "Marielle Macé",
    date : "2017",
    citations: [
        {
            motsCles: ["cabane"],
            content: "Faire des cabanes : imaginer des façons de vivre dans un monde abîmé. p14",
        }, 
        {
        motsCles: ["cabane"],
        content: "Faire des cabanes en tous genres – inventer, jardiner les possibles ; sans craindre d’appeler « cabanes » des huttes de phrases, de papier, de pensée, d’amitié, des nouvelles façons de se représenter l’espace, le temps, l’action,les liens, les pratiques. Faire des cabanes pour occuper autrement le terrain ; c’est-à-dire toujours, aujourd’hui, pour se mettre à plusieurs. p15", 
        },

        {
            motsCles: ["cabane"],
            content: "Faire des cabanes sans forcément tenir à sa cabane – tenir à sa fragilité ou la rêver en dur, installée, éternisable –, mais pour élargir les formes de vie à considérer, retenter avec elles des liens, des côtoiements, des médiations, des nouages. Faire des cabanes pour relancer l’imagination, élargir la zone à défendre, car « de la ZAD », c’est-à-dire de la vie à tenir en vie, il y en a un peu partout sur notre territoire (rappelle Sébastien Thierry). p15", 
         },

         {
            motsCles: ["cabane"],
            content: "Faire des cabanes alors : jardiner des possibles. Prendre soin de ce qui se murmure, de ce qui se tente, de ce qui pourrait venir et qui vient déjà : l’écouter venir, le laisser pousser, le soutenir. p23", 
         },  

         {
            motsCles: ["marge"],
            content: "Ces milieux qui émergent sans programme et vivent en marge des zones d’aménagement urbain ou d’exploitation agricole, ces fragments du« Jardin planétaire » constitués par l’ensemble discontinu, en liberté, indécidé, et très pluriel, des lieux délaissés (« délaissés urbains », c’est comme cela qu’on les appelle, mais aussi friches, talus, landes, lisières...)qui accueillent une diversité écologique surprenante, à laquelle ils font refuge, elle qui partout ailleurs est chassée par les aménagements mêmes. p9", 
         },  

         {
            motsCles: ["cabane"],
            content: " Elles ne sont pas solides, elles ne sont pas durables, très rarement monumentales, de plus en plus souvent illégales, généralement en marge [...]; à une époque où leur existence même est menacée, que ce soit par l’aménagement du territoire et l’urbanisme, la déforestation, les lois et normes de sécurité, elles n’ont jamais autant habité nos pages, nos écrans, nos rêveries. (Julien Zerbone).p31", 
         },     
    ],
};

const livre2 = {
    titre: "Quand le document fait société",
    auteur: "Anne Cordier",
    date: 2019,
    citations: [
        {
            motsCles: ["documentation"],
            content: "La question documentaire continue de se poser, car l'information ne s'est pas affranchie des dimensions matérielle et sociale […]. Ce que nous devons décrire, c'est donc moins la mutation du document en information, que les traits d'une nouvelle économie documentaire. Yves Jeanneret. p21", 
         },  

         {
            motsCles: ["document"],
            content: "'un document est une relation sociale instituée'. Évelyne Broudoux, 2018. p22", 
         },  

         {
            motsCles: ["savoirs"],
            content: "Annette Béguin signale que tout 'être culturel' matérialisant une relation entre des savoirs et des individus est aussi « le signe des relations de pouvoir, de luttes plus ou moins explicites et des influences liées à la circulation des connaissances. p22", 
         }, 
         
         {
            motsCles: ["document"],
            content: "le document est la forme matérielle d’une mémoire de données, objet informationnel qui bénéficie des propriétés de synchronie et de stabilité dans le temps. p23", 
         },  

         {
            motsCles: ["document"],
            content: "agir par le document, autour de lui et sur lui est un acte profondément communicationnel et social, inscrit dans une dimension politique forte : c’est trouver place dans un espace social, contribuer au développement de cet espace social par son engagement dans un collectif. p25", 
         }, 
         
         {
            motsCles: ["document"],
            content: "Pour autant, que les promesses techniques s’incarnent ou non dans des réalités sociales, c’est la puissance du questionnement politique que cristallisent le document, son élaboration, sa diffusion, comme son appropriation, qui nous frappe ici. « Faire société » par et autour du document aujourd’hui, c’est (re)poser des questions fondamentales : quelle société voulons-nous ? Quelle conception des savoirs et avec eux des droits humains, dont celui de l’accès à la connaissance, défendons-nous ? Quelles médiations sommes-nous prêt·es à concevoir pour faire œuvre commune dans une société menacée par l’éparpillement des connaissances comme des collectifs ? p25",
            
         }, 

         {
            motsCles: ["document"],
            content: "Nous cherchons à mettre en lumière les liens entre document et rapports sociaux, considérant ce dernier comme élément vecteur de sociabilités, de socialisations et structuration forte d’identité des acteurs : autour du document on négocie, on s’accorde – ou non – sur des démarches, des procédures, mais aussi plus largement une vision du monde. p26",
            
         }, 
        
         
         {
            motsCles: ["document"],
            content: "Par le cadre sociotechnique dans lequel il s’inscrit les formes d’énonciation éditoriale qu’il implique et les formats de connaissance qu’il engendre, le document, vu comme un dispositif, se trouve être générateur de sociabilités tout autant que de socialisations, deux processus qui parallèlement permettent aux acteurs de prendre en main ces dispositifs, voire de configurer l’activité informationnelle qui est la leur selon leurs besoins et leurs projets, faisant de ces dispositifs info-communicationnels des espaces 'où produire (leur) vouloir'. p26 ",
            
         }, 

         

         
    ],
    
    

};

const livre3 = {
    titre : "Qu'est-ce qu'un dispositif ?",
    auteur: "Giorgio Agamben",
    date : "2014",
    citations: [
        {
            motsCles: ["dispositif"],
            content: "'un ensemble résolument hétérogène comportant des discours, des institutions, des aménagements architecturaux, des décisions réglementaires, des lois, des mesures administratives, des énoncés scientifiques, des propositions philosophiques, morales, philanthropique.' Foucault, 1977. p9",
        },

        {
            motsCles: ["dispositif"],
            content: "'Le dispositif lui-même c'est le réseau qu'on établit entre ces éléments [...] par dispositif, j'entends une sorte - disons - de formation qui, à un moment donné, a eu pour fonction majeure de répondre à une urgence.' Foucault, 1977. p9",
        },

        {
            motsCles: ["dispositif"],
            content: "'C'est ça le dispositif: des stratégies de rapports de force supportant des types de savoir, et supportés par eux.' Foucault, 1977. p9",
        },

        {
            motsCles: ["dispositif"],
            content: "Le dispositif pris en lui-même est le réseau qui s'établit entre ces éléments. p10",
        },
    ],
};

const livre4 = {
  
    citations: [
        {
            motsCles: ["marge"],
            content: "La marge s'annote, elle permet de discuter en dehors du texte. La marge peut-être un espace de discussion, d'échanges et de partage.",
        },
    ],
};


const livre5 = {
    titre :"Les Communs",
    auteur: "Édouard Jourdain",
    date: "2021",
    citations: [
        {
            motsCles: ["communs"],
            content: "Le commun est alors une dynamique qui conduit à envisager une démocratie radicale, aussi bien sur le plan économique que politique, critique à la fois du capitalisme et de l'État. p12",
        },

        {
            motsCles: ["communs"],
            content: "Nous proposons de concevoir synthétiquement les communs comme des institutions gouvernées par les parties prenantes liées à une chose commune ou partagée (matérielle ou immatérielle) au service d'un objet social, garantissant collectivement les capacités et les droits fondamentaux (accès, gestion et décision) des parties à l'égard de la chose ainsi que leurs devoirs (préservation, ouverture et enrichissement) envers elle. p13",
        },

        {
            motsCles: ["biens communs"],
            content: "Les biens communs sont en outre tous les biens matériels et immatériels qui se basent sur une participation collective en termes de production, d'accès, de gestion, de contrôle et de protection des biens eux-mêmes. cité par D. Festa dans le Dictionnaire des biens communs, p301.",
        },

        {
            motsCles: ["biens communs"],
            content: "Les biens communs ont été pensés 'à partir des besoins communs qui fondent le lien social et la société sur un territoire occupé et habité en commun'.p13",
        },
    ],
};


const biblio = [livre1, livre2, livre3, livre4, livre5];

function rechercherMotCle(mot) {
    const results = [];
    biblio.forEach((livre) => {
        let obj = {
            titre: livre.titre,
            auteur: livre.auteur,
            date: livre.date,
            citationsCorrespondantes: [],
        };
        livre.citations.forEach((citation) => {
            citation.motsCles.forEach((motCle) => {
                if (motCle.includes(mot)) {
                    obj.citationsCorrespondantes.push(citation.content);
                }
            });
        });
        if (obj.citationsCorrespondantes.length) results.push(obj);
    });
    return results;
}

function handleClick() {
    const elem = document.getElementById("motCle");
    console.log(elem.value);
    const results = rechercherMotCle(elem.value);
    console.log("résultats", results);
    afficherResultats(results);
}

function afficherResultats(resultats) {
    //récupérer la div "#resultats"
    const div = document.getElementById("resultats");
    //supprimer le contenu de la div
    div.innerHTML = "";

    resultats.forEach((result) => {
        //creer des <p> contenant chaque résultat
        const container = document.createElement("div");

        const paragraphe = document.createElement("info");
        paragraphe.innerHTML = `<ul><li>${result.titre}</li><li>${result.auteur}</li><li>${result.date}</li>`;

        //ajouter ces <p> à la div resultats
        container.appendChild(paragraphe);

        const citationsParagraphes = [];
        result.citationsCorrespondantes.forEach((citation) => {
            const citationP = document.createElement("citationp");
            citationP.classList.add("cit");
            citationP.innerHTML = citation;
            citationsParagraphes.push(citationP);
        });

        citationsParagraphes.forEach((citationParagraphe) => {
            container.appendChild(citationParagraphe);
        });

        div.appendChild(container);
    });
}

function rechercherEtAfficher(mot) {
    const resultats = rechercherMotCle(mot);
    afficherResultats(resultats);
}

function listerMotsCles() {
    const motsCles = [];
    biblio.forEach((livre) => {
        livre.citations.forEach((citation) => {
            citation.motsCles.forEach((motCle) => {
                //virer doublons
                if (motsCles.includes(motCle)) return;
                motsCles.push(motCle);
            });
        });
    });
    return motsCles.sort((a, b) => {
        if (a.toLowerCase() < b.toLowerCase()) {
            return -1;
        }
        if (a.toLowerCase() > b.toLowerCase()) {
            return 1;
        }
        return 0;
    });
}

function motCleClic(mot) {
    rechercherEtAfficher(mot);
    document.getElementById("motCle").value = mot;
}

function afficherMotsCles() {
    const tousLesMotsCles = listerMotsCles();
    const container = document.getElementById("motsCles");

    let elementsAAjouter = [];
    let motsClesTries = [];
    let premierMot;

    //récupérer tous les mots communs à plusieurs mots clés et les stocker dans motsClesTries sous la forme
    /*
        {
            mot: 'absolutisme',
            enfants: []
        }
    */
    tousLesMotsCles.forEach((motCle) => {
        const tmpPremierMot = motCle.split(" ")[0];
        if (
            tmpPremierMot === premierMot &&
            !motsClesTries.find((motCle) => motCle.mot === premierMot)
        ) {
            motsClesTries.push({ mot: premierMot, enfants: [] });
        }
        premierMot = tmpPremierMot;
    });

    //on remplit le tableau des enfants
    /*
        {
            mot: 'absolutisme',
            enfants: ['culturel', 'ethnique']
        }
    */
    //et on ajoute les mots uniques sans enfants
    /*
        {
            mot: 'abstrait',
            enfants: []
        }
    */
    tousLesMotsCles.forEach((motCle) => {
        const tmpPremierMot = motCle.split(" ")[0];
        const existant = motsClesTries.find(
            (motCleTrie) => motCleTrie.mot === tmpPremierMot
        );
        if (existant) {
            const suite = motCle.split(" ").splice(1).join(" ");
            if (suite) existant.enfants.push(suite);
        } else {
            motsClesTries.push({ mot: motCle, enfants: [] });
        }
    });

    //on re-trie les mots clés
    motsClesTries.sort((a, b) => {
        if (a.mot.toLowerCase() < b.mot.toLowerCase()) {
            return -1;
        }
        if (a.mot.toLowerCase() > b.mot.toLowerCase()) {
            return 1;
        }
        return 0;
    });

    //on ajouter ces mots clés à la page
    function creerElement(mot, motARechercher, estDecale = false) {
        const nouvelElement = document.createElement("h3");
        if (estDecale) {
            nouvelElement.classList.add("decale");
        }
        nouvelElement.innerHTML = mot;
        nouvelElement.onclick = () => {
            motCleClic(motARechercher || mot);
        };
        return nouvelElement;
    }

    function creerElements(motCleTrie) {
        const elements = [];
        //ajout de l'élément HTML "mot"
        elements.push(creerElement(motCleTrie.mot));
        //ajout des éléments HTML "enfants"
        motCleTrie.enfants.forEach((enfant) => {
            elements.push(creerElement(enfant, motCleTrie.mot + " " + enfant, true));
        });
        return elements;
    }

    motsClesTries.forEach((motCleTrie) => {
        creerElements(motCleTrie).forEach((nouvelElement) => {
            container.appendChild(nouvelElement);
        });
    });
}
afficherMotsCles();
